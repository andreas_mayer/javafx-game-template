package de.andreasmayer;

import de.andreasmayer.model.Food;
import de.andreasmayer.model.Model;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.awt.*;

public class Graphics {


    // Eigenschaften

    private GraphicsContext gc;
    private Model model;


    // Konstruktoren

    public Graphics(GraphicsContext gc, Model model) {
        this.gc = gc;
        this.model = model;
    }


    // Methoden


    public void draw() {

        gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);

        // Player1 sichtbar machen

        gc.setFill(Color.BLUE);
        gc.fillOval(model.getPlayer1().getX(), model.getPlayer1().getY(),
                model.getPlayer1().getW(), model.getPlayer1().getH());

        // Player2 sichtbar machen

        gc.setFill(Color.GREEN);
        gc.fillOval(model.getPlayer2().getX(), model.getPlayer2().getY(),
                model.getPlayer2().getW(), model.getPlayer2().getH());


        // Food sichtbar machen

        gc.setFill(Color.RED);
        gc.fillOval(model.getFood().getX(), model.getFood().getY(),
                model.getFood().getW(), model.getFood().getH());


        // Game Over

       // if (Model.isGameOver()) {

            gc.setFont(new Font("", 50));
            if (model.getPlayer2().getW() == 0) {

                gc.setFill(Color.BLUE);
                gc.fillText("Player 1 hat gewonnen", 350.0, 300.0);

                gc.setFill(Color.BLACK);
                gc.setFont(new Font("Restart: PRESS RETURN", 50));
                gc.fillText("Restart... PRESS RETURN", 350, 500);



            } else if (model.getPlayer1().getW() == 0) {

                gc.setFill(Color.GREEN);
                gc.fillText("Player 2 hat gewonnen", 350, 300);

                gc.setFill(Color.BLACK);
                gc.setFont(new Font("Restart: PRESS RETURN", 50));
                gc.fillText("Restart... PRESS RETURN", 350, 500);
            }




      //  }

    }

}
