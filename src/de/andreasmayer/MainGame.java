package de.andreasmayer;

import de.andreasmayer.model.Model;
import javafx.application.Application;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

public class MainGame extends Application {

    private Model model;

    @Override
    public void start(Stage stage) throws Exception {

        Model model = new Model();
        Canvas canvas = new Canvas(model.WIDTH, model.HEIGHT);
        Group group = new Group();
        group.getChildren().add(canvas);

        Scene scene = new Scene(group);

        stage.setScene(scene);

        GraphicsContext gc = canvas.getGraphicsContext2D();

        Graphics graphics = new Graphics(gc, model);


        // Schreibt alle Tasten die gedrückt sind in eine ArrayList und löscht sie wieder beim los lassen

        ArrayList<String> input = new ArrayList<String>();
        scene.setOnKeyPressed(
                e ->
                {
                    String code = e.getCode().toString();

                    if (!input.contains(code))
                        input.add(code);
                });

        scene.setOnKeyReleased(
                e ->
                {
                    String code = e.getCode().toString();
                    input.remove(code);
                }
        );

        Timer timer = new Timer(graphics, model, input);
        timer.start();

//        scene.setOnKeyPressed(
//                event -> inputHandler.onKeyPressed(event.getCode())
//        );

        stage.show();

    }
}
