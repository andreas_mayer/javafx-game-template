package de.andreasmayer;

import de.andreasmayer.model.Model;
import javafx.animation.AnimationTimer;

import java.util.ArrayList;

public class Timer extends AnimationTimer {


    // Eigenschaften

    private Graphics graphics;
    private Model model;

    private ArrayList<String> input;


    // Konstruktoren

    public Timer(Graphics graphics, Model model, ArrayList<String> input) {
        this.graphics = graphics;
        this.model = model;
        this.input = input;
    }


    // Methoden

    @Override
    public void handle(long nowNano) {

        model.update(nowNano);
        graphics.draw();


        // Abfrage, welche Tasten gerade gedrückt sind (bzw. sich in der ArrayList befinden)

        if (input.contains("UP")) {
            model.getPlayer1().move(0, -5);
        }
        if (input.contains("DOWN")) {
            model.getPlayer1().move(0, 5);
        }
        if (input.contains("LEFT")) {
            model.getPlayer1().move(-5, 0);
        }
        if (input.contains("RIGHT")) {
            model.getPlayer1().move(5, 0);
        }
        if (input.contains("W")) {
            model.getPlayer2().move(0, -5);
        }
        if (input.contains("S")) {
            model.getPlayer2().move(0, 5);
        }
        if (input.contains("A")) {
            model.getPlayer2().move(-5, 0);
        }
        if (input.contains("D")) {
            model.getPlayer2().move(5, 0);
        }
        if(input.contains("ENTER")){
            model.restartGame();
        }

        System.out.println(input.toString());



    }
}
