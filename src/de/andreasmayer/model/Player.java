package de.andreasmayer.model;

public class Player {


    // Eigenschaften

    private int x;
    private int y;
    private int h;
    private int w;

    private int distance;


    // Konstruktoren

    public Player(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 40;
        this.w = 40;
    }


    // Methoden

    public void grow() {

        this.h += 10;
        this.w += 10;
    }

    public void powerGrow(){

        this.h += 20;
        this.w += 20;
    }

    public void shrink() {

        this.h -= 10;
        this.w -= 10;
    }

    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public void setH(int h) {
        this.h = h;
    }

    public void setW(int w) {
        this.w = w;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

 }
