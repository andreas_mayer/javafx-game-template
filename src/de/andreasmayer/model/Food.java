package de.andreasmayer.model;

public class Food {


    // Eigenschaften

    private int x;
    private int y;
    private int h;
    private int w;

    // Konstruktoren

    public Food() {
        x = (int) (Math.random() * 1180) + 1;
        y = (int) (Math.random() * 780) + 1;
        h = 25;
        w = 25;
    }


    // Methoden

    public int getX() {

        return x;
    }

    public int getY() {

        return y;
    }

    public int getH() {

        return h;
    }

    public int getW() {

        return w;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}
