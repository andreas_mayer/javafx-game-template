package de.andreasmayer.model;

import java.util.Random;

public class Model {


    // Eigenschaften

    private Player player1;
    private Player player2;

    private Food food;

    private double distancePlayer1Food;
    private double distancePlayer2Food;
    private double distancePlayer1Player2;
    //  private double distance

    private boolean collision;
    private boolean isGameOver;


    public final int WIDTH = 1200;
    public final int HEIGHT = 800;


    // Konstruktoren

    public Model() {

        this.player1 = new Player(1100, 400);
        this.player2 = new Player(100, 400);
        this.food = new Food();
        collision = false;
        isGameOver = false;
    }


    // Methoden

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public Food getFood() {

        return food;
    }

    public static boolean isGameOver() {

        return true;
    }

    public void restartGame(){

        player1 = new Player(1100, 400);
        player2 = new Player(100, 400);
        food = new Food();
        collision = false;
        isGameOver = false;
    }

    /*************************************************************/
    // Abstände berechnen

    public void distancePlayer1Food() {

        int dx = (player1.getX() + player1.getW() / 2) - (food.getX() + food.getW() / 2);
        int dy = (player1.getY() + player1.getH() / 2) - (food.getY() + food.getH() / 2);
        distancePlayer1Food = Math.sqrt(dx * dx + dy * dy);
    }

    public void distancePlayer2Food() {

        int dx = (player2.getX() + player2.getW() / 2) - (food.getX() + food.getW() / 2);
        int dy = (player2.getY() + player2.getH() / 2) - (food.getY() + food.getH() / 2);
        distancePlayer2Food = Math.sqrt(dx * dx + dy * dy);
    }

    public void distancePlayer1Player2() {

        int dx = (player1.getX() + player1.getW() / 2) - (player2.getX() + player2.getW() / 2);
        int dy = (player1.getY() + player1.getH() / 2) - (player2.getY() + player2.getH() / 2);
        distancePlayer1Player2 = Math.sqrt(dx * dx + dy * dy);
    }


    /***************************************************************************/
    // Collision Detection zwischen Player - Food / Player1 - Player2

    public void collisionDetectionPlayer1Food() {

        if (distancePlayer1Food < ((player1.getW() / 2) + (food.getW() / 2))) {

            //Zufällig wachsen oder schrumpfen

            if (new Random().nextBoolean()) {

                if(new Random().nextBoolean()){

                    player1.grow();
                }

                else {

                    player1.powerGrow();
                }

            }

            else {

                player1.shrink();
            }

            collision = true;

            food.setX((int) (Math.random() * 1180) + 1);
            food.setY((int) (Math.random() * 780) + 1);

        }
    }

    public void collisionDetectionPlayer2Food() {

        if (distancePlayer2Food < ((player2.getW() / 2) + (food.getW() / 2))) {

            //Zufällig wachsen oder schrumpfen

            if (new Random().nextBoolean()) {

                if(new Random().nextBoolean()){

                    player2.grow();
                }

                else{

                    player2.powerGrow();
                }

            }

            else {

                player2.shrink();
            }

            collision = true;


            // Neue Position von "Food" wenn es gefressen wurde (collision mit Player)

            food.setX((int) (Math.random() * 1180) + 1);
            food.setY((int) (Math.random() * 780) + 1);

        }
    }

    public void collisionDetectionPlayer1Player2() {

        if (distancePlayer1Player2 < ((player1.getW() / 2) + (player2.getW() / 2))) {

            if (player1.getW() > player2.getW()) {

                player2.setW(0);
              //  isGameOver();


            } else if (player1.getW() < player2.getW()) {

                player1.setW(0);
              //  isGameOver();
            }
        }
    }


    public boolean isCollision() {

        return collision;
    }


    /************************************************************************/
    // Collision Detection GameField

    public void collisionDetectionGameField() {

        if (player1.getX() <= 0) {

            player1.setX(1);
        }

        else if (player1.getX() + player1.getW() >= WIDTH) {

            player1.setX(WIDTH - player1.getW());

        }

        else if (player1.getY() <= 0) {

            player1.setY(1);
        }

        else if (player1.getY() >= HEIGHT - player1.getW()) {

            player1.setY(HEIGHT - (player1.getW()));
        }

        if (player2.getX() <= 0) {

            player2.setX(1);
        }

        else if (player2.getX() + player2.getW() >= WIDTH) {

            player2.setX(WIDTH - (player2.getW()));
        }

        else if (player2.getY() <= 0) {

            player2.setY(1);
        }

        else if (player2.getY() >= HEIGHT - player2.getW()) {

            player2.setY(HEIGHT - (player2.getW()));
        }

    }


    /***************************************************************************/

    // Wird vom Timer in der Handle Methode aufgerufen

    public void update(long nowNano) {


        distancePlayer1Food();
        distancePlayer2Food();
        distancePlayer1Player2();
        collisionDetectionPlayer1Food();
        collisionDetectionPlayer2Food();
        collisionDetectionPlayer1Player2();
        collisionDetectionGameField();

        //System.out.println((player1.getW() / 2) + player2.getW() / 2);
        System.out.println(distancePlayer1Player2);
        // isCollision();

        player1.move(0, 0);
    }

    public double getWIDTH() {
        return WIDTH;
    }

    public double getHEIGHT() {
        return HEIGHT;
    }
}
